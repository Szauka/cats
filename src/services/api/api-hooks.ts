import { ConfigurationParameters } from "./configuration";
import { ImagesApi, FavouritesApi, VotesApi } from "./api";
import { useEffect, useState } from "react";

const appConfig: ConfigurationParameters = {
  basePath: "https://api.thecatapi.com/v1",
  apiKey: "6698e31a-f866-42d2-af48-bc5fe5cbcb40",
};

export function useCatImages() {
  const api = new ImagesApi(appConfig);

  const [images, setImages] = useState([]);
  const [status, setStatus] = useState("idle");

  useEffect(() => {
    const fetchData = async () => {
      setStatus("fetching");

      const response = await api.imagesGet(
        appConfig.apiKey,
        "application/json",
        100
      );
      const data = await response.json();

      setImages(data);
      setStatus("fetched");
    };

    fetchData();
  }, []);

  return { status, images };
}

export function useUploadCatImages() {
  const api = new ImagesApi(appConfig);

  const [error, setError] = useState(null);
  const [status, setStatus] = useState("idle");

  async function uploadImage(file) {
    setStatus("uploading");

    const formData = new FormData();
    formData.append("file", file, "file");

    try {
      await api.imagesUploadPost(appConfig.apiKey, formData);
      setStatus("uploaded");
    } catch (response) {
      setStatus("error");
    }
  }

  const reset = () => setStatus("idle");

  return { status, error, uploadImage, reset };
}

export type Favourite = { id: string, image_id: string };
export type Favourites = Record<string, Favourite>;

export function useFavourites() {

  const api = new FavouritesApi({
    basePath: "https://api.thecatapi.com/v1",
    apiKey: "fc4467ba-d43a-4ae5-b5aa-9fe74955ea49",
  });

  const [status, setStatus] = useState("idle");
  const [favourites, setFavourites] = useState<Favourites>({});

  const fetchData = async () => {
    setStatus("fetching");
    try {
      const response = await api.favouritesGet(
        appConfig.apiKey,
        "application/json"
      );
      const parsedResponse = await response.json();
      const favourites: Favourites = {};
      for (const favourite of parsedResponse) {
        favourites[favourite.image_id] = favourite;
      }
      setFavourites(favourites);
      setStatus("fetched");
    } catch (response) {
      setStatus("error");
      throw response;
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  async function makeFavourite(imageId: string) {
    try {
      await api.favouritesPost(
        appConfig.apiKey,
        {
          image_id: imageId,
        },
        "application/json"
      );
      fetchData();
    } catch (reason) {
      setStatus("error");
      fetchData();
      throw reason;
    }
  }

  async function unfavourite(imageId: string) {
    try {
      let favourite = favourites[imageId];
      await api.favouritesfavouriteIdDelete(
        appConfig.apiKey,
        favourite.id,
        "application/json"
      );
      fetchData();
    } catch (reason) {
      setStatus("error");
      fetchData();
      throw reason;
    }
  }

  return {
    favourites,
    makeFavourite,
    unfavourite,
  };
}

export function useVotes() {
  const api = new VotesApi(appConfig);
  const [votes, setVotes] = useState([]);
  const [status, setStatus] = useState("idle");

  async function getVotes() {
    setStatus("fetching");

    try {
      const response = await api.votesGet(appConfig.apiKey);
      const data = await response.json();
      setVotes(data);
      setStatus("fetched");
    } catch (response) {
      setStatus("error");
      throw response;
    }
  }

  useEffect(() => {
    getVotes();
  }, []);

  function getVotesForCat(imageId: string) {
    const votesOnImage = votes.filter((vote) => {
        return vote.image_id === imageId;
    });
    
    let score = 0;
    for (const vote of votesOnImage) {
      score += vote.value > 0 ? 1 : -1;
    }

    return score;
  }

  async function vote(imageId: string, type: "up" | "down") {
    let value = type === "up" ? 1 : 0;
    try {
      await api.votesPost(
        appConfig.apiKey,
        {
          image_id: imageId,
          value,
        },
        "application/json"
      );
      getVotes();
    } catch (response) {
      setStatus(response);
      throw response;
    }
  }

  async function upvote(imageId: string) {
    await vote(imageId, "up");
  }

  async function downvote(imageId: string) {
    await vote(imageId, "down");
  }

  return {
    status,
    votes,
    upvote,
    downvote,
    getVotesForCat
  };
}
