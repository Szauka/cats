import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import { useCatImages, useUploadCatImages, useFavourites, useVotes } from './services/api/api-hooks';

jest.mock('./services/api/api-hooks');

describe('App', () => {

  beforeEach(() => {
    useCatImages.mockReturnValue({
      status: 'fetched',
      images: [],
    });
    useUploadCatImages.mockReturnValue({
      status: 'fetched',
      error: null,
      uploadImage: jest.fn(),
    });
    useFavourites.mockReturnValue({
      status: 'fetched',
      favourite: true,
      makeFavourite: jest.fn(),
      unfavourite: jest.fn(),
    });
    useVotes.mockReturnValue({
      status: 'fetched',
      votes: [],
      upvote: jest.fn(),
      downvote: jest.fn(),
    });
  });

  test('render the app', () => {
    const { baseElement } = render(<App />);

    expect(baseElement).toMatchSnapshot('Whole App');
  });
});
