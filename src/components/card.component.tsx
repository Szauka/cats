import React, { useEffect } from "react";
import { Card, Image, Button, Icon } from "semantic-ui-react";
import { useVotes } from "../services/api/api-hooks";

export interface ICatCard {
  image: string;
  favorite: boolean;
  makeFavourite: (imageId: string) => void;
  unfavourite: (imageId: string) => void;
  upvote: (imageId: string) => void;
  downvote: (imageId: string) => void;
  score: number;
  id: string;
}

function CatCard(props: ICatCard) {

  const onFavorite = () => {
    if (props.favorite) {
      props.unfavourite(props.id);
    } else {
     props.makeFavourite(props.id);
    }
  };

  const onVoteUp = () => {
    props.upvote(props.id);
  };

  const onVoteDown = () => {
    props.downvote(props.id);
  };

  useEffect(() => {});

  return (
    <Card link>
      <Image
        wrapped
        ui={false}
        src={props.image}
        label={{
          as: "a",
          onClick: onFavorite,
          corner: "right",
          icon: <Icon name={"heart"} color={props.favorite ? "red" : "grey"} />,
        }}
      ></Image>
      <Card.Content>
        <Card.Description>
          <Icon name="star outline" />
          Score: {props.score ? props.score : 0}
        </Card.Description>
      </Card.Content>

      <Card.Content extra>
        <Button.Group fluid>
          <Button
            content="Vote up"
            icon="arrow up"
            labelPosition="left"
            onClick={onVoteUp}
            color="green"
          />
          <Button
            onClick={onVoteDown}
            color="red"
            content="Vote down"
            icon="arrow down"
            labelPosition="right"
          />
        </Button.Group>
      </Card.Content>
    </Card>
  );
}

export default CatCard;
