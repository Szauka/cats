import React, { useState, useRef } from "react";
import { NavLink } from "react-router-dom";
import {
  Button,
  Container,
  Header,
  Icon,
  Input,
  Message,
  Segment,
  Form,
  Progress,
} from "semantic-ui-react";
import { useUploadCatImages } from "../services/api/api-hooks";

function Upload() {
  const inputCatUploadRef = useRef(null);
  const { status, error, uploadImage, reset } = useUploadCatImages();

  const openFileUploadDialog = () => {
    inputCatUploadRef.current.click();
  };

  return (
    <Container>
      <Segment placeholder>
        {status === "idle" && (
          <>
            <Header icon>
              <Icon name="file image outline" />
              Upload your cat image.
            </Header>
            <Button onClick={openFileUploadDialog} primary>
              Upload your cat picture
            </Button>
          </>
        )}

        {status === "uploading" && (
          <>
            <Header icon>
              <Icon name="file image outline" />
              Your cat is being uploaded
            </Header>

            <Progress percent={100} indicating />
          </>
        )}

        {status === "error" && (
          <>
            <Header icon>
              <Icon name="file image outline" />
              Something went wrong our side
            </Header>
            <Button onClick={reset} green>
              Try again
            </Button>
          </>
        )}

        {status === "uploaded" && (
          <>
            <Header icon>
              <Icon name="file image outline" />
              Your cat has been uploaded!
            </Header>
            <Button as={NavLink} to={'/'} green>
              Go vote for your cat
            </Button>
          </>
        )}
      </Segment>
      <input
        type="file"
        id="file"
        ref={inputCatUploadRef}
        style={{ display: "none" }}
        onChange={(e) => {
          uploadImage(e.target.files[0]);
        }}
      />
    </Container>
  );
}

export default Upload;
