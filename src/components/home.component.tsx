import { Card, Container, Loader } from "semantic-ui-react";
import CatCard from "./card.component";
import { useCatImages, useFavourites, useVotes } from "../services/api/api-hooks";

function Home() {
  const { images, status } = useCatImages();
  const { favourites, makeFavourite, unfavourite } = useFavourites();
  const { getVotesForCat, votes, downvote, upvote } = useVotes();

  return (
    <Container>
      <h1>Welcome to Dimi's cats</h1>
      <p>Here you can vote and/or upload your pictures of your cats!</p>

      {status !== "fetched" && (
        <Loader active indeterminate>
          Getting cats
        </Loader>
      )}

      {status === "fetched" && (
        <Card.Group centered stackable>
          {images.map((cat) => {
            const isFavourite = !!favourites[cat.id];
            return (
              <CatCard
                key={cat.id}
                id={cat.id}
                image={cat.url}
                favorite={isFavourite}
                makeFavourite={makeFavourite}
                unfavourite={unfavourite}
                downvote={downvote}
                upvote={upvote}
                score={getVotesForCat(cat.id)}
              />
            );
          })}
        </Card.Group>
      )}
    </Container>
  );
}

export default Home;
