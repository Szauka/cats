import React from "react";
import Home from "./components/home.component";
import { Menu } from "semantic-ui-react";
import { BrowserRouter, Switch, Route, NavLink } from "react-router-dom";
import "semantic-ui-css/semantic.min.css";
import Upload from "./components/upload.component";

function App() {
  return (
    <BrowserRouter>
      <Menu stackable fluid>
        <Menu.Item as={NavLink} exact to='/' name="Dimi's cats" header></Menu.Item>
        <Menu.Menu position="right">
          <Menu.Item as={NavLink} to='/upload' name="upload" >Upload a cat</Menu.Item>
        </Menu.Menu>
      </Menu>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>

        <Route exact path="/upload">
          <Upload />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
